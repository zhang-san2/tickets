import Vue from 'vue'
import App from './App'
import https from 'pages/api/request.js'

Vue.config.productionTip = false

App.mpType = 'app'
Vue.prototype.$https = https;

Vue.prototype.$title = '/api/';

import uView from "uview-ui";
Vue.use(uView);


const app = new Vue({
    ...App
})
app.$mount()
