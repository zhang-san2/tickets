// vue.config.js
module.exports = {
  devServer : {
      https : false,
      port: 80,
      disableHostCheck: true,
      proxy: {
          '/api/': {
              target: "http://m.scczyxhh.com:8007",
              changeOrigin: true,
              // pathRewrite:{"^/api/v1/":""}
          }
      }
  }
}
