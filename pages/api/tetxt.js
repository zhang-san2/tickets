location ^~/api/v1/ {
      rewrite  ^/api/(.*)$ /$1 break; 
      proxy_pass http://m.scczyxhh.com:8007;
      proxy_redirect off;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
}