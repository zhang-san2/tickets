const baseUrl = 'http://m.scczyxhh.com:8007/PayArea/';    //请求地址
const httpRequest = (opts, data) => {
    let httpDefaultOpts = {
        url: baseUrl+opts.url,
        data: data,
        beforeSend :function(xmlHttp){
            xmlHttp.setRequestHeader("If-Modified-Since","0"); 
            xmlHttp.setRequestHeader("Cache-Control","no-cache");
        },
        method: opts.method,
        header: opts.method == 'GET' ? {
        'X-Requested-With': 'XMLHttpRequest',
        "Accept": "application/json",
        "Content-Type": "application/json; charset=UTF-8"
    } : {
       'content-type': 'application/x-www-form-urlencoded'
    },
        dataType: 'json',
    }
    let promise = new Promise(function(resolve, reject) {
        uni.request(httpDefaultOpts).then(
            (res) => {
                resolve(res)
            }
        ).catch(
            (response) => {
                reject(response)
            }
        )
    })
    return promise
};
export default {
    baseUrl,
    httpRequest
}